DEVELOPMENT ROUND TEMPLATE  

## Introduction (Round 0)
<b>Discipline | <b>chemical science
:--|:--|
<b> Lab | <b> chemistry lab.
<b> Experiment|     <b> Determination of copper by Spectrocolorimetry.
<h5> About the Lab (Objective) : </h5>
The objective of this lab to determine the concentration of copper in given solution.
<h5> About the Experiment: </h5>
This experiment is about calculation of copper concentration by Spectrocolorimeter.
<h5> Target Audience: </h5>
 UG .
<h5> Course Alignment: </h5>
The experiment is design for first year UG students of the university
<h5> Universities Mapped : </h5>
AKTU
<b>Name of Developer | <b> Manoj Kumar Yadava
:--|:--|
<b> Institute | <b> Rajkiya Engineering College Banda
<b> Email id|     <b> manojyadava16@gmail.com
<b> Department | Applied Science and Humanities

#### Mentor Details

<b>Mentored by | <b> Er Sheetal Singh
:--|:--|
<b> Institute | <b> IIT Kanpur
<b> Email id|     <b> sheetals@iitk.ac.in
<b> Department | Computer Science

#### Contributors List

SrNo | Name | Faculty or Student | Department| Institute | Email id
:--|:--|:--|:--|:--|:--|
1 | Manoj Kumar Yadava | Faculty | Applied Science and Humanities | Rajkiya Engineering College Banda, Banda | manojyadava16@gmail.com
2 | Nitish Kumar | Student | Mechanical Engineering| Rajkiya Engineering College Banda, Banda |nitishkumar29400@gmail.com
3 | Amitabh Shahi | Student | Information technology | Rajkiya Engineering College Banda, Banda |amitabhshahi09@gmail.com
4 | Akash Singh | Student | Information technology | Rajkiya Engineering College Banda, Banda |1907340130009@recbanda.ac.in
5 | Akash Sachan | Student | Information technology | Rajkiya Engineering College Banda, Banda |akashsachanboss@gmail.com


<br>

----------------------------------------------*******************-----------------------------------------------


## Pedagogy (Round 1)
<p align="center">

<br>
<br>
<b> Determination of copper by Spectrocolorimetry.<a name="top"></a> <br>
</p>

<b>Discipline | <b>chemical science
:--|:--| 
<b> Lab | <b> chemistry Lab.
<b> Experiment| <b> Determination of copper by Spectrocolorimetry.




<a name="LO"></a>
#### 1. Focus Area : 
                                 1.Reinforce theoretical concept
                                 2.Experimentation
 #### 2. Learning Objectives and Cognitive Level


Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>identify the apparatus and reagents  being used in the experiment. <br>  | Remember   |Identify
2.| User will be able to: <br>describe  the "preparation of sample" for the experiment. <br> | Understand | Explain 
3.| User will be able to: <br>Apply the experimental conditions by setting maximum wavelength and placing sample in spectro calorimeter <br> | Apply  | Apply
4.| User will be able to: <br>perform experiment and record the reading for further calculation. <br> |  Apply | Calculate
5.| User will be able to: <br>sketch the calibration graph <br> |  Apply | sketch
6.| User will be able to: <br>determine the concentration of copper in water sample <br> |  Evaluate | Determine



<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="IS"></a>
#### 3. Instructional Strategy
###### Name of Instructional Strategy :  
  <u> Expository
###### Assessment Method:
 Formative Assessment
<br>
Question will be asked during the experiment to make the user accustomed to the experiment.

<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="AQ"></a>
#### 4. Task & Assessment Questions:
<br>

Sr. No |	Learning Objective	| Task to be performed by <br> the student in the simulator | Assessment Questions as per LO & Task
:--|:--|:--|:-:
1.| Students will be able to identify the apparatus and reagents  being used in the experiment. | Student has to select the  required apperatus and  reagents from the list display by the simulator. | Select all the apperatus and reagents required for the experiment ? 
2.| Students will be able to describe  the "preparation of sample" for the experiment. | Student has to prepare the sample using reagents. | Which reagent is added very first to the sample?.
3.| Students will be able to Apply the experimental conditions by setting maximum wavelength and placing sample in spectro calorimeter. | Student has to set maximum wavelenght and place the sample in spectrocolorimeter. | What  is the maximum wavelenght at which experiment is being performed? <br> A 350nm <br> B 450nm <br> C 480nm <br> D. none of the above  <br>
4.| Student will able to perform experiment and record the reading for further calculation| Student will use the spectrocolorimeter and record the reading.  |  What is the mean of the optical densities of all the samples ? <br> A. 0.2g <br> B. 0.32g <br> C. 0.5g <br>  D. 1.0g  <br>
5.| Students will be able to sketch the calibration graph. | Student has to press the "plot graph" button . |  Is the plotted graph is straight line? (Yes/No).
6.| Students will be able to determine the concentration of copper in water sample. | Student has to press on "show result" button. | what is the permissible limit of copper for drinking water ? <br> A. 0.2 ppm <br> B. 3.2 ppm <br> C. 1.3 ppm <br>  D. 4.0 ppm </b> <br>.
 
 


 
<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="SI"></a>

#### 4. Simulator Interactions:
<br>

Sr.No | What Students will do? |	What Simulator will do?	| Purpose of the task
:--|:--|:--|:--:
1.| Student has to select the  required apperatus and  reagents from the list display by the simulator| Simulator will display apperatus and reagents list and give instruction " select the apperatus and reagents required for the experiment".| To make user remember the requisites of the experimetent.
2.| Student will be able to describe  the "preparation of sample" for the experiment | Simulator will display selected apperatus and reagents and display instruction to mis reagents for sample sample preperation| To make student used to with the sample preperation.
3.| Student has to set maximum wavelenght at which experiment is being performed and place the sample in spectrocolorimeter |Simulator will popup istruction to set wavelength and place sample in spectrocolorimeter| To make student known with the maximum wavelength on which experiment is performed.
4.| Student will use the spectrocolorimeter and record the reading. | Simulator will allow the student to record the reading| To make the student accustemed in recording the reading.
5.| Student has to press the "plot graph" button and give the answer of the question | Simulator will display "plot graph" button and  popup the question "is the plotted graph is straight line?" (Yes/No).| To check whether the plotted graph is correct .
6.| Student has to press on "show result" button and answer the question |Simulator will display "show result" button and popup the question "what is the permissible limit of copper for drinking water ?" <br> A. 0.2 ppm <br> B. 3.2 ppm <br> C. 1.3 ppm <br>  D. 4.0 ppm | To make the Students well known with the permissible limit of copper for drinking water. </b> <br>.

Round 2:
**1. Story outline**

This experiment is about determining copper concentration in a given water. It involves preparing solution by adding 0.2 g of CuSO4 into a 100 ml SMF and made upto the mark using distilled water. A drop of concentrated H2SO4 is added to prevent precipitation of Cu(OH)2. This solution is approximately diluted to 10 times in a 100 ml SMF. 1 ml of this solution is taken in a test tube. 5 ml of 10% NH4NO3 and 1 ml of 4% K4[Fe(CN)6] are added and made up to 15 ml. The optical density of this solution is measured using a spectrocolorimeter after fixing maximum wavelength at 480 nm. Similar measurements are made with 2, 3, 4, 5, 6 and 7 ml of standard CuSO4 solution and the calibration line is obtained. From the calibration line, the amount of copper (II) present in the unknown is determined using its optical density. 
</b> <br>
**2. Story**

 **2.1 Set the visual stage description**

**A.** preparing the standard solution:
The set up of the experiment consist of spectrocalorimeter, beaker, test tube, pipette, etc .
While preparing the standard solution, 0.2 g of CuSO4 into a 100 ml SMF and made upto the mark using distilled water. A drop of concentrated H2SO4 is added to prevent precipitation of Cu(OH)2. This solution is approximately diluted to 10 times in a 100 ml SMF. 1 ml of this solution is taken in a test tube. 5 ml of 10% NH4NO3 and 1 ml of 4% K4[Fe(CN)6] are added and made up to 15 ml

**2.2 user objective and goals**

 **A.** To give the identification of  the apparatus and reagents being used in the experiment..<br>
 **B.** To provide understanding of “preparation of standard solution” for the experiment.<br>
 **C.** To assess the experimental condition required for the experiment.<br>
 **D.** To provide virtual practice of operating spectrocalorimeter.</b> <br>

**2.3 Set the Pathway Activities**
**A.** There will be red button to start the simulator. The set up of the experiment consist of spectrocalorimeter, beaker, test tube, pipette, etc. will display by the simulator.<br>
**B.** There will be button to add the reagents to prepare standard solution. After preparing standard solution the next button will take over to the next set up.<br>
**C.** There will be button for taking sample in test tube. Then user will press next for spectrocalorimeter set up.<br>
**D.** now there will be button for power on , placing test tube on spectrocalorimeter, setting wavelength, and see reading.<br>
**E.** Once the reading of optical densities for all the seven samples are noted. Simulator will display table.<br>
**F.** user will fill all the reading of optical densities then plot the calibrated graph will display <br>
**G.** now there will be “show result” button which will display the result of the experiment.</b> <br>

**2.4 Set challenges and question/complexity/variation in question**
{
  question: " What is the formula of absorbance?",  
  answers: {
    a: " A=log(I0/I) ",                  
    b: " A=log(I0-I) ",                  
    c: " A=log(I0*I) ",                  
    d: " None of the above "                   
  },
  correctAnswer: "a"               
},

{
  question: " what is the maximum wavelength to be fixed in spectrocolorimeter?<img src=’images/fig.PNG’>",  
  answers: {
    a: " 420 nm ",                  
    b: " 500 nm ",                  
    c: " 480 nm",                  
    d: " 400 nm"                  
  },
  correctAnswer: "c"               
},                    
{
  question: " The optical density is linearly proportional to_______?",  
  answers: {
    a: " wavelength",                  
    b: " concentration",                  
    c: " weight",                  
    d: " absorbance"                   
  },
  correctAnswer: "b"               
},
    {
  question: " The graph is plotted between.?",  
  answers: {
    a: " wavelength vs concentration",                  
    b: " absorbance vs wavelength",                  
    c: " wavelength vs optical density",                  
    d: " absorbance vs concentration"                   
  },
  correctAnswer: "d"               
},





2.5 Allow pitfalls:**

This pitfall does not mean wrong answer and retrying . It is designed to clear misconceptions or incorrect knowledge .


**2.5 Conclusion**

The time required to recall the apparatus & reagents and understand the procedure of the experiment is 5-7 minutes. It will take another 5 min to standard solution. Data recording will take 5 min. filling reading of optical densities in the table and plotting calibrated graph will take 3-5min. , result and assessment question will also take 5 min. Hence total time in performing the experiment will be almost 30 min.

**2.6 Equations/Formulas**

**A.** Equation will be used for the calculation of:

**a)**  concentration of copper = Test OD/Std OD * Std Concentration







Round 3:

   